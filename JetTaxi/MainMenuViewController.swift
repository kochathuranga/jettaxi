//
//  MainMenuViewController.swift
//  JetTaxi
//
//  Created by Mudith Chathuranga on 8/14/17.
//  Copyright © 2017 Chathuranga. All rights reserved.
//

import UIKit

class MainMenuViewController: SwiftySideMenuViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        SwiftySideMenuInfo.shared.swiftySideMenu = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK: - SwiftySideMenuDataSource

extension MainMenuViewController: SwiftySideMenuDataSource {
    
    func menuNavigationTabs(_ sideView: UIView) -> [SwiftySideMenuChildViewControllers] {
        var childViews: [SwiftySideMenuChildViewControllers] = []
        
        let childView1 = SwiftySideMenuChildViewControllers()
        childView1.tabName = "Home"
        childView1.viewControllerIdentifier = "MainHomeNav"
        childViews.append(childView1)
        
        let childView2 = SwiftySideMenuChildViewControllers()
        childView2.tabName = "Ride History"
        childView2.viewControllerIdentifier = "RideHistoryNav"
        childViews.append(childView2)
        
        let childView3 = SwiftySideMenuChildViewControllers()
        childView3.tabName = "Customer Support"
        childView3.viewControllerIdentifier = "CustomerSupportNav"
        childViews.append(childView3)
        
        return childViews
    }
    
    func menuNavigationTabs(subviewFor sideMenu: UIView) -> UIView {
        let view = UIView()
        return view
    }
    
}


