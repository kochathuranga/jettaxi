//
//  AddLocationPointsViewController.swift
//  JetTaxi
//
//  Created by Mudith Chathuranga on 8/15/17.
//  Copyright © 2017 Chathuranga. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleMaps
import GooglePlaces

class AddLocationPointsViewController: UIViewController {
    
    
    @IBOutlet weak var searchTextBox: UITextField!
    @IBOutlet weak var searchTextRightAnchor: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var centerLocationPin: UIImageView!
    
    @IBOutlet weak var textClearButton: UIButton!
    @IBOutlet weak var locationTitle: UILabel!
    
    
    var timer = Timer()
    var searchResults: [LocationPoint] = []
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 17.0
    var selectedPlace: GMSPlace? // The currently selected place.
    
    var selectedLocationID: String!
    var selectedLat: NSNumber!
    var selectedLng: NSNumber!
    var selectedLocationName: String!
    
    var isUserClickedSuggestion: Bool = false
    var isPickupLocation: Bool = true
    
    var homeVC: HomeMapViewController!
    var controllerTitle: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchTextBox.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        self.tableView.tableFooterView = UIView()
        self.tableView.layer.cornerRadius = 6.0
        self.textClearButton.alpha = 0
        self.textClearButton.layer.cornerRadius = 6.0
        self.searchTextRightAnchor.constant = 16.0
        self.locationTitle.text = self.controllerTitle
        
        self.locationManager = CLLocationManager()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.distanceFilter = 50
        self.locationManager.startUpdatingLocation()
        self.locationManager.delegate = self
        self.placesClient = GMSPlacesClient.shared()
        
        let camera = GMSCameraPosition.camera(withLatitude: 1.354666, longitude: 103.868552, zoom: 15.0)
        self.googleMapView.camera = camera
        self.googleMapView.isMyLocationEnabled = true
        self.googleMapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.googleMapView.isHidden = true
        // Do any additional setup after loading the view.
        
        Reachability.shared.events.listenTo(eventName: "isUnreachable") {
            self.disconnected()
        }
        Reachability.shared.events.listenTo(eventName: "isReachable") {
            self.connected()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Reachability.shared.events.removeListeners(eventNameToRemoveOrNil: "isUnreachable")
        Reachability.shared.events.removeListeners(eventNameToRemoveOrNil: "isReachable")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backToHomeView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearText(_ sender: Any) {
        self.searchTextBox.text = nil
        self.tableViewHeight.constant = 0
    }
    
    @IBAction func setLocationPoint(_ sender: Any) {
        if isPickupLocation {
            homeVC.pickupLat = self.selectedLat
            homeVC.pickupLng = self.selectedLng
            homeVC.isChangedPickup = true
            homeVC.pickupLocationText.text = self.selectedLocationName
        } else {
            homeVC.dropoffLat = self.selectedLat
            homeVC.dropoffLng = self.selectedLng
            homeVC.dropoffLocationText.text = self.selectedLocationName
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func connected() {
        print("CONECTED")
    }
    
    func disconnected() {
        print("DISCONNECTED")
    }
    
    
    func getSearchResults() {
        
        self.timer.invalidate()
        let sendingText = self.searchTextBox.text?.replacingOccurrences(of: " ", with: "%20")
        
        Alamofire.request(DropMeConnect.googleAutoCompleteURL + "&input=\(sendingText!)", method: .get, encoding: URLEncoding.httpBody, headers: DropMeConnect.headersWithOutToken)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    
                    if let value = response.result.value {
                        let json = JSON(value)
                        if json["status"] == "OK" {
                            let predications = json["predictions"]
                            var searchResultsTmp: [LocationPoint] = []
                            for item in 0...predications.count - 1 {
                                let searchPoint = LocationPoint()
                                searchPoint.ID = predications[item]["place_id"].stringValue
                                searchPoint.locationName = predications[item]["structured_formatting"]["main_text"].stringValue 
                                searchPoint.address = predications[item]["structured_formatting"]["secondary_text"].stringValue 
                                searchResultsTmp.append(searchPoint)
                            }
                            self.searchResults = searchResultsTmp
                            searchResultsTmp.removeAll()
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        
                        } else if json["status"] == "ZERO_RESULTS" {
                            print("NO RESULTS")
                            self.searchResults.removeAll()
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        } else {
                            print("Error in Google places API :-  \(json["status"])")
                            self.searchResults.removeAll()
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                    }
                    
                case .failure:
                    
                    if response.response?.statusCode != nil {
                        
                        if let data = response.data {
                            self.searchResults.removeAll()
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
        }
    }
    
    func getLocationIDInfo() {
        
        self.searchTextBox.text = "Fetching Location...."
        Alamofire.request(DropMeConnect.googlePlaceIDSearchURL + "&placeid=\(self.selectedLocationID!)", method: .get, encoding: URLEncoding.httpBody, headers: DropMeConnect.headersWithOutToken)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    
                    if let value = response.result.value {
                        let json = JSON(value)
                        if json["status"] == "OK" {
                            self.selectedLng = json["result"]["geometry"]["location"]["lng"].numberValue
                            self.selectedLat = json["result"]["geometry"]["location"]["lat"].numberValue
                            if self.selectedLocationName! != json["result"]["vicinity"].stringValue && json["result"]["vicinity"].stringValue != "" {
                                self.selectedLocationName = "\(self.selectedLocationName!), \(json["result"]["vicinity"].stringValue)"
                            }
                            
                            DispatchQueue.main.async {
                                self.searchTextBox.text = self.selectedLocationName
                                let camera = GMSCameraPosition.camera(withLatitude: self.selectedLat as! CLLocationDegrees,
                                                                      longitude: self.selectedLng as! CLLocationDegrees,
                                                                      zoom: self.zoomLevel)
                                    self.googleMapView.animate(to: camera)
                                
                            }
                            
                        } else if json["status"] == "ZERO_RESULTS" {
                            print("NO RESULTS")
                            self.selectedLocationID = nil
                            DispatchQueue.main.async {
                               
                            }
                        } else {
                            print("Error in Google places API :-  \(json["status"])")
                            self.selectedLocationID = nil
                            DispatchQueue.main.async {
                               
                            }
                        }
                    }
                    
                case .failure:
                    
                    if response.response?.statusCode != nil {
                        
                        if let data = response.data {
                            self.searchResults.removeAll()
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
        }
    }

}

// MARK: - UITextViewDelegate

extension AddLocationPointsViewController: UITextFieldDelegate {
    
    func textFieldDidChange(textField: UITextField) {
        self.timer.invalidate()
        if (textField.text?.characters.count)! > 0 {
            self.timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.getSearchResults), userInfo: nil, repeats: true)
        } else {
            self.searchResults.removeAll()
            self.tableView.reloadData()
        }

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchTextBox.endEditing(true)
        self.tableViewHeight.constant = 0
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.searchTextBox.textAlignment = NSTextAlignment.center
        UIView.animate(withDuration: 0.5) { 
            self.textClearButton.alpha = 0
            self.searchTextRightAnchor.constant = 16.0
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.searchTextBox.textAlignment = NSTextAlignment.left
        UIView.animate(withDuration: 0.5) {
            self.textClearButton.alpha = 1
            self.searchTextRightAnchor.constant = 70.0
        }
    }
    
}


// MARK: - UITableViewDataSource

extension AddLocationPointsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchResults.count == 0 {
            self.tableViewHeight.constant = 0
        } else {
            self.tableViewHeight.constant = CGFloat(40.0 * Double(self.searchResults.count)) + 10.0
        }
        return self.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationSearchResultTableViewCell", for: indexPath) as! LocationSearchResultTableViewCell
        cell.locationName.text = self.searchResults[indexPath.row].locationName
        cell.locationAddress.text = self.searchResults[indexPath.row].address
        return cell
    }
}

// MARK: - UITableViewDataSource

extension AddLocationPointsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedLocationID = nil
        self.selectedLocationID = self.searchResults[indexPath.row].ID
        self.tableViewHeight.constant = 0
        self.selectedLocationName = "\(self.searchResults[indexPath.row].locationName!)"
        self.searchTextBox.text = self.selectedLocationName
        self.searchTextBox.endEditing(true)
        if self.selectedLocationID != nil {
            self.isUserClickedSuggestion = true
            self.getLocationIDInfo()
        }
    }

}


// MARK: - CLLocationManagerDelegate

extension AddLocationPointsViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.searchTextBox.endEditing(true)
        self.tableViewHeight.constant = 0
        self.centerLocationPin.image = UIImage(named: "searchingPin")
        if !self.isUserClickedSuggestion {
            self.searchTextBox.text = "Fetching Location...."
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.centerLocationPin.image = UIImage(named: "locationPin")
        self.reverseGeocodeCoordinate(coordinate: position.target)
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                
                let lines = address.lines!
                print(lines.joined(separator: "\n"))
                self.selectedLat = coordinate.latitude as NSNumber
                self.selectedLng = coordinate.longitude as NSNumber
                
                if !self.isUserClickedSuggestion {
                    if address.thoroughfare != nil && address.locality != nil {
                        self.selectedLocationName = "\(address.thoroughfare ?? ""), \(address.locality ?? "")"
                        self.searchTextBox.text = "\(address.thoroughfare ?? ""), \(address.locality ?? "")"
                    } else if address.locality != nil {
                        self.selectedLocationName = "Unnamed Road, \(address.locality ?? "")"
                        self.searchTextBox.text = self.selectedLocationName
                    } else {
                        self.selectedLocationName = "Unnamed Road, Unknown City"
                        self.searchTextBox.text = self.selectedLocationName
                    }
                    
                } else {
                    self.isUserClickedSuggestion = false
                }
            }
        }
    }
}

// MARK: - CLLocationManagerDelegate

extension AddLocationPointsViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        if self.googleMapView.isHidden {
            self.googleMapView.isHidden = false
            self.googleMapView.camera = camera
        } else {
            self.googleMapView.animate(to: camera)
        }
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            self.googleMapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}



