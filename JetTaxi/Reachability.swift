//
//  Reachability.swift
//  JetTaxi
//
//  Created by Mudith Chathuranga on 8/24/17.
//  Copyright © 2017 Chathuranga. All rights reserved.
//

import Foundation
import SystemConfiguration


open class Reachability {
    
    open static let shared = Reachability()
    let events = EventManager()
    
    open let hostname: String = "https://apis.google.com/js/api.js"
    open let refreshTime: Double = 3.0
    private var timer = Timer()
    
    private init() {
        self.timer = Timer.scheduledTimer(timeInterval: self.refreshTime, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    deinit {
        self.timer.invalidate()
    }
    
    private func getConnectionStatus(completionHandler:@escaping (ConnectivityType) -> ()) {
//        let urlString = URL(string: hostname)
//        if let url = urlString {
//            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
//                if error != nil {
//                    completionHandler(.unreachable)
//                } else {
//                    if data != nil {
//                        completionHandler(.reachable)
//                    }
//                }
//            }
//            task.resume()
//        }
        
        let url4 = URL(string: hostname)!
        let session4 = URLSession.shared
        let request = NSMutableURLRequest(url: url4)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let paramString = ""
        request.httpBody = paramString.data(using: String.Encoding.utf8)
        let task = session4.dataTask(with: request as URLRequest) { (data, response, error) in
            guard let _: Data = data, let _: URLResponse = response, error == nil else {
                completionHandler(.unreachable)
                return
            }
            completionHandler(.reachable)
        }
        task.resume()
    }
    
    @objc private func timerAction() {
        self.getConnectionStatus { (status) in
            if status == .unreachable {
                self.events.trigger(eventName: "isUnreachable", information: "")
            } else {
                self.events.trigger(eventName: "isReachable", information: "")
            }
        }
    }
    
}

public enum ConnectivityType {
    case unreachable
    case reachable
}
