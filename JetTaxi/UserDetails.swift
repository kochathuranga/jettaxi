//
//  UserDetails.swift
//  JetTaxi
//
//  Created by Mudith Chathuranga on 8/15/17.
//  Copyright © 2017 Chathuranga. All rights reserved.
//

import Foundation

class UserDetails {
    static let userName = "USERNAME"
    static let userID = "USERID"
    static let userSession = "USERSESSION"
    static let userState = "USERSTATE"
    static let userCity = "USERCITY"
    static let defaults = UserDefaults.standard
    
    static func storeData(data: String?, key: String) {
        defaults.setValue(data, forKey: key)
        defaults.synchronize()
    }
    
    static func getData(key: String) -> String {
        if let userData = defaults.string(forKey: key) {
            return userData // Some String Value
        } else {
            return ""
        }
    }
    
    static func removeAll() {
        self.storeData(data: nil, key: self.userName)
        self.storeData(data: nil, key: self.userID)
        self.storeData(data: nil, key: self.userSession)
        self.storeData(data: nil, key: self.userCity)
    }
}
