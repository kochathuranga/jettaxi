//
//  LocationPoint.swift
//  JetTaxi
//
//  Created by Mudith Chathuranga on 8/15/17.
//  Copyright © 2017 Chathuranga. All rights reserved.
//

import Foundation
import UIKit

class LocationPoint: NSObject {
    
    var ID: String?
    var locationName: String?
    var address: String?

}
