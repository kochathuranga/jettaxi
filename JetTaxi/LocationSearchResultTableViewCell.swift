//
//  LocationSearchResultTableViewCell.swift
//  JetTaxi
//
//  Created by Mudith Chathuranga on 8/15/17.
//  Copyright © 2017 Chathuranga. All rights reserved.
//

import UIKit

class LocationSearchResultTableViewCell: UITableViewCell {

    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var locationAddress: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
