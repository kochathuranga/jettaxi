//
//  DropMeConnect.swift
//  JetTaxi
//
//  Created by Mudith Chathuranga on 8/15/17.
//  Copyright © 2017 Chathuranga. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DropMeConnect {
    
    static var BaseURL: String! = "http://droptaxilk.com/include/view/"
    
    static var signInURL: String {
        get {
            return BaseURL + "auth/token"
        }
    }
    
    static var googleAutoCompleteURL: String {
        get {
            return "https://maps.googleapis.com/maps/api/place/autocomplete/json?components=country:lk&key=AIzaSyBBqUHuTUmvu7KRJN4Y3Ovik-Akd8feWxs"
        }
    }
    
    static var googlePlaceIDSearchURL: String {
        get {
            return "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyBBqUHuTUmvu7KRJN4Y3Ovik-Akd8feWxs"
        }
    }
    
    static var headersWithOutToken: HTTPHeaders {
        get {
            return [
                "Content-Type": "application/x-www-form-urlencoded",
                "Accept": "application/json"
            ]
        }
    }
    
    
}
