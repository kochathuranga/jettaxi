//
//  HomeMapViewController.swift
//  JetTaxi
//
//  Created by Mudith Chathuranga on 8/8/17.
//  Copyright © 2017 Chathuranga. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class HomeMapViewController: UIViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var menuNavigationView: UIView!
    @IBOutlet weak var pickUpPointView: UIView!
    @IBOutlet weak var dropOffPointView: UIView!
    
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var centerLocationPin: UIImageView!
    
    @IBOutlet weak var bookNowView: GradientView!
    @IBOutlet weak var taxiLogo: UIImageView!
    
    
    @IBOutlet weak var pickupLocationText: UILabel!
    @IBOutlet weak var dropoffLocationText: UILabel!
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    var selectedPlace: GMSPlace? // The currently selected place.
    var selectedVehicleType: Int = 10
    
    var didLoadMap: Bool = false
    var pickupLat: NSNumber!
    var pickupLng: NSNumber!
    var dropoffLat: NSNumber!
    var dropoffLng: NSNumber!
    
    var isChangedPickup: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.googleMapView.delegate = self
        SwiftySideMenuImageView.addMenuImageView(addMenuViewTo: menuNavigationView)
        
        self.locationManager = CLLocationManager()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.distanceFilter = 50
        self.locationManager.startUpdatingLocation()
        self.locationManager.delegate = self
        self.placesClient = GMSPlacesClient.shared()
        
        let camera = GMSCameraPosition.camera(withLatitude: 1.354666, longitude: 103.868552, zoom: 15.0)
        self.googleMapView.camera = camera
        self.googleMapView.isMyLocationEnabled = true
        self.googleMapView.settings.myLocationButton = true
        self.googleMapView.padding = UIEdgeInsetsMake(0, 0, 100.0, 0)
        self.googleMapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.googleMapView.isHidden = true
        
        self.pickUpPointView.layer.cornerRadius = 6.0
        self.dropOffPointView.layer.cornerRadius = 6.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.isChangedPickup {
            self.isChangedPickup = false
            let camera = GMSCameraPosition.camera(withLatitude: self.pickupLat as! CLLocationDegrees,
                                                  longitude: self.pickupLng as! CLLocationDegrees,
                                                  zoom: zoomLevel)
            self.googleMapView.animate(to: camera)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let x: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 100.0, height: 100.0))
        x.backgroundColor = UIColor.blue
        return x
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.centerLocationPin.image = UIImage(named: "searchingPin")
        self.pickupLocationText.text = "Fetching Location...."
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, delay: 0, options: [], animations: {
                self.bookNowView.center.y = self.view.bounds.size.height + 60
                self.taxiLogo.center.y = -50
                self.taxiLogo.alpha = 0
                self.menuNavigationView.center.y = -40
                self.pickUpPointView.center.y = 40
                self.dropOffPointView.center.y = 40
                self.dropOffPointView.alpha = 0
            }, completion: nil)
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.centerLocationPin.image = UIImage(named: "locationPin")
        self.reverseGeocodeCoordinate(coordinate: position.target)
        UIView.animate(withDuration: 0.3, delay: 0, options: [], animations: {
            self.bookNowView.center.y = self.view.bounds.size.height - 60
            self.taxiLogo.center.y = 50
            self.taxiLogo.alpha = 1
            self.menuNavigationView.center.y = 40
            self.pickUpPointView.center.y = 100
            self.dropOffPointView.center.y = 148
            self.dropOffPointView.alpha = 1
        }, completion: nil)
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()

        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                
                let lines = address.lines!
                print(lines.joined(separator: "\n"))
//                self.pickupLocationText.text = "\(address.thoroughfare!), \(address.locality ?? "")"
                self.pickupLat = coordinate.latitude as NSNumber
                self.pickupLng = coordinate.longitude as NSNumber
                
                if address.thoroughfare != nil && address.locality != nil {
                    self.pickupLocationText.text = "\(address.thoroughfare ?? ""), \(address.locality ?? "")"
                } else if address.locality != nil {
                    self.pickupLocationText.text = "Unnamed Road, \(address.locality ?? "")"
                } else {
                    self.pickupLocationText.text = "Unnamed Road, Unknown City"
                }
            }
        }
    }
    
    
    @IBAction func showDropOffLocation(_ sender: Any) {
        self.performSegue(withIdentifier: "showLocationPicker", sender: 2)
    }
    
    @IBAction func showPickupLocation(_ sender: Any) {
        self.performSegue(withIdentifier: "showLocationPicker", sender: 1)
    }
    
    
    @IBAction func clearDropOffPoint(_ sender: Any) {
        self.dropoffLat = nil
        self.dropoffLng = nil
        self.dropoffLocationText.text = "Enter Drop off location"
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLocationPicker" {
            let destinationVC = segue.destination as! AddLocationPointsViewController
            destinationVC.homeVC = self
            if sender as! Int == 1 {
                destinationVC.isPickupLocation = true
                destinationVC.controllerTitle = "Pickup Point"
            } else {
                destinationVC.isPickupLocation = false
                destinationVC.controllerTitle = "Drop off Point"
            }
        }
    }
    
}

extension HomeMapViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !self.didLoadMap {
            self.didLoadMap = true
            let location: CLLocation = locations.last!
            self.reverseGeocodeCoordinate(coordinate: location.coordinate)
            
            
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                  longitude: location.coordinate.longitude,
                                                  zoom: zoomLevel)
            if self.googleMapView.isHidden {
                self.googleMapView.isHidden = false
                self.googleMapView.camera = camera
            } else {
                self.googleMapView.animate(to: camera)
            }
        }
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            self.googleMapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}


// MARK: - UICollectionViewDataSource

extension HomeMapViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VehicleTypeCollectionViewCell", for: indexPath) as! VehicleTypeCollectionViewCell
        
        if indexPath.row == 0 {
            cell.vehicleName.text = "Tuk Tuk"
            if self.selectedVehicleType == indexPath.row {
                cell.vehicleImg.image = UIImage(named: "tuktukSelected")
                cell.vehicleName.textColor = UIColor.white
            } else {
                cell.vehicleImg.image = UIImage(named: "tuktukNormal")
                cell.vehicleName.textColor = UIColor.black
            }
        } else if indexPath.row == 1 {
            cell.vehicleName.text = "Mini Car"
            if self.selectedVehicleType == indexPath.row {
                cell.vehicleImg.image = UIImage(named: "nanocarSelected")
                cell.vehicleName.textColor = UIColor.white
            } else {
                cell.vehicleImg.image = UIImage(named: "nanocarNormal")
                cell.vehicleName.textColor = UIColor.black
            }
        } else if indexPath.row == 2 {
            cell.vehicleName.text = "Car"
            if self.selectedVehicleType == indexPath.row {
                cell.vehicleImg.image = UIImage(named: "minicarSelected")
                cell.vehicleName.textColor = UIColor.white
            } else {
                cell.vehicleImg.image = UIImage(named: "minicarNormal")
                cell.vehicleName.textColor = UIColor.black
            }

        } else if indexPath.row == 3 {
            cell.vehicleName.text = "Van"
            if self.selectedVehicleType == indexPath.row {
                cell.vehicleImg.image = UIImage(named: "vanSelected")
                cell.vehicleName.textColor = UIColor.white
            } else {
                cell.vehicleImg.image = UIImage(named: "vanNormal")
                cell.vehicleName.textColor = UIColor.black
            }
        } else {
            cell.vehicleName.text = "Lorry"
            if self.selectedVehicleType == indexPath.row {
                cell.vehicleImg.image = UIImage(named: "lorrySelected")
                cell.vehicleName.textColor = UIColor.white
            } else {
                cell.vehicleImg.image = UIImage(named: "lorryNormal")
                cell.vehicleName.textColor = UIColor.black
            }
        }
        return cell
    }

}

// MARK: - UICollectionViewDelegate

extension HomeMapViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedVehicleType = indexPath.row
        collectionView.reloadData()
    }
}
