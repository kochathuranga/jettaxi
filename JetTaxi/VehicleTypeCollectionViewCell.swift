//
//  VehicleTypeCollectionViewCell.swift
//  JetTaxi
//
//  Created by Mudith Chathuranga on 8/10/17.
//  Copyright © 2017 Chathuranga. All rights reserved.
//

import UIKit

class VehicleTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var vehicleImg: UIImageView!
    @IBOutlet weak var vehicleName: UILabel!
    
    
}
